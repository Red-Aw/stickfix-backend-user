<?php

use App\Models\User;
use App\Models\Role;
use App\Http\Controllers\RoleController;
use Illuminate\Support\Facades\Hash;

class UserTest extends TestCase
{
    public function testCreateUserUnauthorized()
    {
        $userData = [
            'username' => 'JohnDoe',
            'email' => 'doe@example.com',
            'password' => 'demo12345',
            'password_confirmation' => 'demo12345',
            'language' => 'lv',
            'isConfirmed' => true,
            'roles' => [],
        ];

        $this->json('POST', 'api/storeUser', $userData, ['Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->clearTableAtEnd();
    }

    public function testCreateUser()
    {
        $userData = [
            'username' => 'DojesJon',
            'email' => 'doje123@example.com',
            'password' => 'test12345',
            'password_confirmation' => 'test12345',
            'language' => 'lv',
            'isConfirmed' => true,
            'roles' => [],
        ];

        $this->json('POST', 'api/storeUser', $userData, [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $this->clearTableAtEnd();
    }

    public function testAlreadyTakenEmail()
    {
        $userData = [
            'username' => 'RandomUser',
            'email' => 'randomuser@example.com',
            'password' => Hash::make('randompassword'),
            'confirmed' => 1,
        ];
        \App\Models\User::factory()->create($userData);

        $data = [
            'email' => 'randomuser@example.com',
        ];

        $this->json('POST', 'api/checkEmailAvailability', $data, [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'data' => [
                    'available' => false,
                ],
            ]);

        $this->clearTableAtEnd();
    }

    public function testAlreadyTakenEmailUnauthorized()
    {
        $userData = [
            'username' => 'RandomUserx',
            'email' => 'randomuserx@example.com',
            'password' => Hash::make('randompassword'),
            'confirmed' => 1,
        ];
        \App\Models\User::factory()->create($userData);

        $data = [
            'email' => 'randomuserx@example.com',
        ];

        $this->json('POST', 'api/checkEmailAvailability', $data, [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->clearTableAtEnd();
    }

    public function testAvailableEmail()
    {
        $userData = [
            'username' => 'RandomUserz',
            'email' => 'randomuserz@example.com',
            'password' => Hash::make('randompassword'),
            'confirmed' => 1,
        ];
        \App\Models\User::factory()->create($userData);

        $data = [
            'email' => 'availableemail@example.com',
        ];

        $this->json('POST', 'api/checkEmailAvailability', $data, [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'data' => [
                    'available' => true,
                ],
            ]);

        $this->clearTableAtEnd();
    }

    public function testAvailableEmailUnauthorized()
    {
        $userData = [
            'username' => 'RandomUsery',
            'email' => 'randomusery@example.com',
            'password' => Hash::make('randompassword'),
            'confirmed' => 1,
        ];
        \App\Models\User::factory()->create($userData);

        $data = [
            'email' => 'availableemail2@example.com',
        ];

        $this->json('POST', 'api/checkEmailAvailability', $data, [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->clearTableAtEnd();
    }

    public function testAlreadyTakenUsername()
    {
        $userData = [
            'username' => 'gwendolyn',
            'email' => 'gwendolyn.bahringer6@gmail.com',
            'password' => Hash::make('gwendolyn123'),
            'confirmed' => 1,
        ];
        \App\Models\User::factory()->create($userData);

        $data = [
            'username' => 'gwendolyn',
        ];

        $this->json('POST', 'api/checkUsernameAvailability', $data, [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'data' => [
                    'available' => false,
                ],
            ]);

        $this->clearTableAtEnd();
    }

    public function testAlreadyTakenUsernameUnauthorized()
    {
        $userData = [
            'username' => 'florencio_haley',
            'email' => 'florencio_haley@hotmail.com',
            'password' => Hash::make('florencio_haley123'),
            'confirmed' => 1,
        ];
        \App\Models\User::factory()->create($userData);

        $data = [
            'username' => 'florencio_haley',
        ];

        $this->json('POST', 'api/checkUsernameAvailability', $data, [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->clearTableAtEnd();
    }

    public function testAvailableUsername()
    {
        $userData = [
            'username' => 'marion_padberg',
            'email' => 'marion_padberg@yahoo.com',
            'password' => Hash::make('marion_padberg123'),
            'confirmed' => 1,
        ];
        \App\Models\User::factory()->create($userData);

        $data = [
            'username' => 'marion_padberg1',
        ];

        $this->json('POST', 'api/checkUsernameAvailability', $data, [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'data' => [
                    'available' => true,
                ],
            ]);

        $this->clearTableAtEnd();
    }

    public function testAvailableUsernameUnauthorized()
    {
        $userData = [
            'username' => 'lurline55',
            'email' => 'lurline55@yahoo.com',
            'password' => Hash::make('lurline55123'),
            'confirmed' => 1,
        ];
        \App\Models\User::factory()->create($userData);

        $data = [
            'username' => 'lurline551',
        ];

        $this->json('POST', 'api/checkUsernameAvailability', $data, [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->clearTableAtEnd();
    }

    public function testChangeUserPasswordUnauthorized()
    {
        $userData = [
            'username' => 'quentin.rogahn26',
            'email' => 'quentin.rogahn26@hotmail.com',
            'password' => Hash::make('quentin.rogahn26123'),
            'confirmed' => 1,
        ];
        $user = \App\Models\User::factory()->create($userData);

        $newPasswordData = [
            'id' => $user->id,
            'currentPassword' => 'quentin.rogahn26123',
            'newPassword' => 'gwendolyn123',
            'newPassword_confirmation' => 'gwendolyn123',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/newPass', $newPasswordData, [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->clearTableAtEnd();
    }

    public function testChangeUserPassword()
    {
        $userData = [
            'username' => 'quentin.rogahn26',
            'email' => 'quentin.rogahn26@hotmail.com',
            'password' => Hash::make('quentin.rogahn26123'),
            'confirmed' => 1,
        ];
        $user = \App\Models\User::factory()->create($userData);

        $newPasswordData = [
            'id' => $user->id,
            'currentPassword' => 'quentin.rogahn26123',
            'newPassword' => 'gwendolyn123',
            'newPassword_confirmation' => 'gwendolyn123',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/newPass', $newPasswordData, [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'data' => [
                    'userId' => $user->id,
                ],
            ]);

        $this->clearTableAtEnd();
    }

    public function testChangeUserLanguage()
    {
        $userData = [
            'username' => 'quentin.rogahn26',
            'email' => 'quentin.rogahn26@hotmail.com',
            'password' => Hash::make('quentin.rogahn26123'),
            'confirmed' => 1,
        ];
        $user = \App\Models\User::factory()->create($userData);

        $newLanguageData = [
            'id' => $user->id,
            'language' => 'en',
        ];

        $this->json('POST', 'api/changeLanguage', $newLanguageData, [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->json('POST', 'api/changeLanguage', $newLanguageData, [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $this->clearTableAtEnd();
    }

    public function testCheckPasswordsMatch()
    {
        $userData = [
            'username' => 'qoberbrunner41',
            'email' => 'zora.oberbrunner41@yahoo.com',
            'password' => Hash::make('oberbrunner41123'),
            'confirmed' => 1,
        ];
        $user = \App\Models\User::factory()->create($userData);

        $passwordData = [
            'id' => $user->id,
            'currentPassword' => 'oberbrunner41123',
        ];

        $this->json('POST', 'api/checkPasswordMatch', $passwordData, [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->json('POST', 'api/checkPasswordMatch', $passwordData, [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'data' => [
                    'match' => true,
                ],
            ]);

        $passwordData = [
            'id' => $user->id,
            'currentPassword' => 'oberbrunner41yxz',
        ];

        $this->json('POST', 'api/checkPasswordMatch', $passwordData, [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'data' => [
                    'match' => false,
                ],
            ]);

        $this->clearTableAtEnd();
    }

    public function testEditUserData()
    {
        $userData = [
            'username' => 'elijah35',
            'email' => 'elijah35@hotmail.com',
            'password' => Hash::make('elijah35123'),
            'confirmed' => 1,
        ];
        $user = \App\Models\User::factory()->create($userData);

        $newUserData = [
            'id' => $user->id,
            'username' => 'frida_heaney67',
            'originalUsername' => 'elijah35',
            'email' => 'frida_heaney67@yahoo.com',
            'originalEmail' => 'elijah35@hotmail.com',
            'language' => 'ru',
            'isConfirmed' => true,
            'roles' => [],
        ];

        $this->json('POST', 'api/editUser', $newUserData, [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->json('POST', 'api/editUser', $newUserData, [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'data' => [
                    'userId' => $user->id,
                ],
            ]);

        $this->clearTableAtEnd();
    }

    public function testMarkUser()
    {
        $userData = [
            'username' => 'stephany93',
            'email' => 'stephany93@gmail.com',
            'password' => Hash::make('stephany93123'),
            'confirmed' => 1,
        ];
        $user = \App\Models\User::factory()->create($userData);

        $this->json('GET', 'api/markUser/' . $user->id, [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->json('GET', 'api/markUser/' . $user->id, [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'data' => [
                    'userId' => $user->id,
                ],
            ]);

        $this->clearTableAtEnd();
    }

    public function testGetUsers()
    {
        $userData = [
            'username' => 'virginia.franecki23',
            'email' => 'virginia.franecki23@yahoo.com',
            'password' => Hash::make('virginia.franecki23123'),
            'confirmed' => 1,
        ];
        \App\Models\User::factory()->create($userData);

        $this->json('GET', 'api/getUsers', [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->json('GET', 'api/getUsers', [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $this->clearTableAtEnd();
    }

    public function testGetUser()
    {
        $userData = [
            'username' => 'ansel.hoeger1',
            'email' => 'ansel.hoeger18@yahoo.com',
            'password' => Hash::make('ansel.hoeger1123'),
            'confirmed' => 1,
        ];
        $user = \App\Models\User::factory()->create($userData);

        $this->json('GET', 'api/getUser/' . $user->id, [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->json('GET', 'api/getUser/' . $user->id, [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $this->clearTableAtEnd();
    }

    public function testGetUserRoles()
    {
        $userData = [
            'username' => 'michel36',
            'email' => 'michel36@gmail.com',
            'password' => Hash::make('michel36123'),
            'confirmed' => 1,
        ];
        $user = \App\Models\User::factory()->create($userData);

        $userRoles = [
            [
                'userId' => $user->id,
                'role' => RoleController::READ_MESSAGES,
            ],
            [
                'userId' => $user->id,
                'role' => RoleController::MARK_USER,
            ],
        ];

        $createdRoles = [];
        foreach ($userRoles as $userRole) {
            $createdRoles[] = \App\Models\Role::factory()->create($userRole);
        }

        $this->json('GET', 'api/getRoles/' . $user->id, [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->json('GET', 'api/getRoles/' . $user->id, [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $this->clearTableAtEnd();
    }

    public function testGetAllRoles()
    {
        $this->json('GET', 'api/getAllRoles', [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->json('GET', 'api/getAllRoles', [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'data' => [
                    'roles' => RoleController::ROLES,
                ],
            ]);

        $this->clearTableAtEnd();
    }

    public function testCheckUserWithSuperAdminRole()
    {
        $userData = [
            'username' => 'amy16',
            'email' => 'amy16@hotmail.com',
            'password' => Hash::make('amy16123$kv'),
            'confirmed' => 1,
        ];
        $user = \App\Models\User::factory()->create($userData);

        $roleData = [
            'userId' => $user->id,
            'role' => RoleController::SUPER_ADMIN,
        ];
        $role = \App\Models\Role::factory()->create($roleData);

        $this->json('GET', 'api/checkUserRole/' . $user->id . '/' . $role->role , [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->json('GET', 'api/checkUserRole/' . $user->id . '/' . $role->role , [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'data' => [
                    'hasRole' => true,
                ],
            ]);

        $this->json('GET', 'api/checkUserRole/' . $user->id . '/' . RoleController::ENTER_ADMIN_CP , [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'data' => [
                    'hasRole' => true,
                ],
            ]);

        $this->clearTableAtEnd();
    }

    public function testCheckOnAuthorizationAsAuthenticatedUserWithAddNewUserRole()
    {
        $userData = [
            'username' => 'landen_olson',
            'email' => 'landen_olson@gmail.com',
            'password' => Hash::make('landen_olson123'),
            'confirmed' => 1,
        ];
        $user = \App\Models\User::factory()->create($userData);

        $roleData = [
            'userId' => $user->id,
            'role' => RoleController::ADD_NEW_USER,
        ];
        $role = \App\Models\Role::factory()->create($roleData);

        $this->json('GET', 'api/checkUserRole/' . $user->id . '/' . $role->role , [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->json('GET', 'api/checkUserRole/' . $user->id . '/' . $role->role , [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'data' => [
                    'hasRole' => true,
                ],
            ]);

        $this->json('GET', 'api/checkUserRole/' . $user->id . '/' . RoleController::SEE_STATISTICS , [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'data' => [
                    'hasRole' => false,
                ],
            ]);

        $this->clearTableAtEnd();
    }

    public function clearTableAtEnd()
    {
        User::where('id', '<>', 1)->delete();
        Role::where('userId', '<>', 1)->delete();
    }
}
