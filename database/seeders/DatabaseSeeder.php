<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\RoleController;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $adminId = DB::table('users')->insertGetId([
            'username' => 'admin',
            'email' => 'admin@stickfix.store',
            'password' => '$2y$10$CEoeQ0DHDR6rVVETsaOlaOuPmyF10Uf4wqr/iZQgDxMLxbUvuwSmy',
            'confirmed' => true,
        ]);

        DB::table('roles')->insert([
            'userId' => $adminId,
            'role' => RoleController::SUPER_ADMIN,
        ]);
    }
}
