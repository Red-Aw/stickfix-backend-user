<?php

namespace Database\Factories;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Carbon\Carbon;

class RoleFactory extends Factory
{
    protected $model = Role::class;

    public function definition(): array
    {
    	return [
            'userId' => User::factory(),
            'role' => 'role',
            'addedAt' => Carbon::now(),
    	];
    }
}
