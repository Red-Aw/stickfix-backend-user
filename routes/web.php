<?php

/** @var \Laravel\Lumen\Routing\Router $router */

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->post('/storeUser', 'UserController@store');
    $router->post('/checkEmailAvailability', 'UserController@checkEmailAvailability');
    $router->post('/checkUsernameAvailability', 'UserController@checkUsernameAvailability');
    $router->post('/newPass', 'UserController@changePass');
    $router->post('/changeLanguage', 'UserController@changeLanguage');
    $router->post('/checkPasswordMatch', 'UserController@checkPasswordMatch');
    $router->get('/getRoles/{id}', 'UserController@getRoles');
    $router->get('/getAllRoles', 'UserController@getAllRoles');
    $router->post('/editUser', 'UserController@edit');
    $router->get('/markUser/{id}', 'UserController@mark');
    $router->get('/getUser/{id}', 'UserController@getUser');
    $router->get('/getUsers', 'UserController@getUsers');
    $router->get('/checkUserRole/{id}/{role}', 'UserController@checkUserRole');
});
