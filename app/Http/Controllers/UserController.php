<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\Models\User;
use App\Models\Role;

use App\Http\Controllers\RoleController;
use App\Http\Controllers\EnumController;

use Carbon\Carbon;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserController extends Controller
{
    private $enumController;

    public function __construct(EnumController $enumController)
    {
        $this->enumController = $enumController;
    }

    public function store(Request $request)
    {
        $rules = [
            'username' => 'required|min:3|max:64|unique:users',
            'email' => 'required|email|max:256|unique:users',
            'password' => 'required|min:8|max:1024|confirmed',
            'language' => 'required|min:2|max:2',
            'isConfirmed' => 'boolean',
            'roles' => 'array',
            'roles.*' => 'required',
        ];

        $messages = [
            'username.required' => 'validation.requiredField',
            'username.min' => 'validation.mustBeAtLeast3CharsLong',
            'username.max' => 'validation.mustBe64CharsOrFewer',
            'username.unique' => 'validation.usernameAlreadyExists',
            'email.required' => 'validation.requiredField',
            'email.email' => 'validation.invalidEmail',
            'email.max' => 'validation.mustBe256CharsOrFewer',
            'email.unique' => 'validation.emailAlreadyExists',
            'password.required' => 'validation.requiredField',
            'password.min' => 'validation.mustBeAtLeast8CharsLong',
            'password.max' => 'validation.mustBe1024CharsOrFewer',
            'password.confirmed' => 'validation.passwordsDoNotMatch',
            'language.required' => 'validation.requiredField',
            'language.min' => 'validation.mustBe2Chars',
            'language.max' => 'validation.mustBe2Chars',
            'isConfirmed.boolean' => 'validation.requiredField',
            'roles.required' => 'validation.requiredField',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, $validator->errors());
        }

        // Validate language
        $isLanguageFound = false;
        foreach ($this->enumController->getLanguages() as $lang) {
            if ($request->language === $lang) {
                $isLanguageFound = true;
                break;
            }
        }

        if (!$isLanguageFound) {
            return $this->response(
                false,
                'error.validationError',
                [],
                Response::HTTP_OK,
                [ 'language' => ['validation.requiredField'] ]
            );
        }

        $user = new User;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->language = $request->language;
        $user->confirmed = $request->isConfirmed;
        $created = $user->save();

        if (!$created) {
            return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, null);
        }

        $roleErrors = RoleController::storeNewUserRoles($request->roles, $user->id);

        return $this->response(true, 'recordAdded', ['userId' => $user->id], Response::HTTP_OK, $roleErrors);
    }

    public function checkEmailAvailability(Request $request)
    {
        $rules = [
            'email' => 'unique:users',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $this->response(true, '', ['available' => false], Response::HTTP_OK, []);
        }

        return $this->response(true, '', ['available' => true], Response::HTTP_OK, []);
    }

    public function checkUsernameAvailability(Request $request)
    {
        $rules = [
            'username' => 'unique:users',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $this->response(true, '', ['available' => false], Response::HTTP_OK, []);
        }

        return $this->response(true, '', ['available' => true], Response::HTTP_OK, []);
    }

    public function changePass(Request $request)
    {
        $rules = [
            'id' => 'required|exists:users',
            'currentPassword' => 'required|min:8|max:1024',
            'newPassword' => 'required|min:8|max:1024|confirmed',
        ];

        $messages = [
            'id.required' => 'validation.identifierDoesNotExist',
            'id.exists' => 'validation.identifierDoesNotExist',
            'currentPassword.required' => 'validation.requiredField',
            'currentPassword.min' => 'validation.mustBeAtLeast8CharsLong',
            'currentPassword.max' => 'validation.mustBe1024CharsOrFewer',
            'newPassword.required' => 'validation.requiredField',
            'newPassword.min' => 'validation.mustBeAtLeast8CharsLong',
            'newPassword.max' => 'validation.mustBe1024CharsOrFewer',
            'newPassword.confirmed' => 'validation.passwordsDoNotMatch',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, $validator->errors());
        }

        $user = User::find($request->id);
        if (!Hash::check($request->currentPassword, $user->password)) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, null);
        }

        $dateTime = Carbon::now();

        $user->password = Hash::make($request->newPassword);
        $user->editedAt = $dateTime->toDateTimeString();
        $user->passEditedAt = $dateTime->toDateTimeString();
        $updated = $user->save();

        if (!$updated) {
            return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, null);
        }

        return $this->response(true, 'dataUpdated', ['userId' => $user->id], Response::HTTP_OK, null);
    }

    public function changeLanguage(Request $request)
    {
        $rules = [
            'id' => 'required|exists:users',
            'language' => 'required|min:2|max:2',
        ];

        $messages = [
            'id.required' => 'validation.identifierDoesNotExist',
            'id.exists' => 'validation.identifierDoesNotExist',
            'language.required' => 'validation.requiredField',
            'language.min' => 'validation.mustBe2Chars',
            'language.max' => 'validation.mustBe2Chars',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, $validator->errors());
        }

        $user = User::find($request->id);

        // Validate language
        $isLanguageFound = false;
        foreach ($this->enumController->getLanguages() as $lang) {
            if ($request->language === $lang) {
                $isLanguageFound = true;
                break;
            }
        }

        if (!$isLanguageFound) {
            return $this->response(
                false,
                'error.validationError',
                [],
                Response::HTTP_OK,
                [ 'language' => ['validation.requiredField'] ]
            );
        }

        $dateTime = Carbon::now();

        $user = User::find($request->id);
        $user->language = $request->language;
        $user->editedAt = $dateTime->toDateTimeString();
        $updated = $user->save();

        if (!$updated) {
            return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, null);
        }

        return $this->response(true, 'dataUpdated', [], Response::HTTP_OK, null);
    }

    public function checkPasswordMatch(Request $request)
    {
        $rules = [
            'id' => 'required|exists:users',
            'currentPassword' => 'required|min:8|max:1024',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $this->response(true, '', ['match' => false], Response::HTTP_OK, []);
        }

        $user = User::find($request->id);
        if (!Hash::check($request->currentPassword, $user->password)) {
            return $this->response(true, '', ['match' => false], Response::HTTP_OK, []);
        }

        return $this->response(true, '', ['match' => true], Response::HTTP_OK, []);
    }

    public function getUsers()
    {
        $users = User::orderBy('id', 'asc')->get();

        return $this->response(true, '', ['users' => $users]);
    }

    public function getUser($id)
    {
        try {
            $user = User::with('roles')->findOrFail($id);
            return $this->response(true, '', ['user' => $user], Response::HTTP_OK, null);
        } catch (ModelNotFoundException $e) {
            return $this->response(false, 'error.recordNotFound', [], Response::HTTP_OK, null);
        }
    }

    public function getRoles($id)
    {
        try {
            $user = User::findOrFail($id);
            $roles = Role::where('userId', $user->id)->get();
            return $this->response(true, '', ['roles' => $roles], Response::HTTP_OK, null);
        } catch (\Exception $e) {
            return $this->response(false, 'error.errorSelectingData', [], Response::HTTP_OK, null);
        }
    }

    public function getAllRoles()
    {
        return $this->response(true, '', ['roles' => RoleController::ROLES], Response::HTTP_OK, null);
    }

    public function mark($id)
    {
        try {
            $user = User::findOrFail($id);
            if ($user->id === 1) {
                throw new Exception();
            }
        } catch (ModelNotFoundException $e) {
            return $this->response(false, 'error.recordNotFound', [], Response::HTTP_OK, null);
        } catch (\Exception $e) {
            return $this->response(false, 'error.error', [], Response::HTTP_OK, null);
        }

        $dateTime = Carbon::now();

        $user->confirmed = !$user->confirmed;
        $user->editedAt = $dateTime->toDateTimeString();
        $updated = $user->save();

        if (!$updated) {
            return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, null);
        }

        return $this->response(true, 'dataUpdated', ['userId' => $user->id], Response::HTTP_OK, null);
    }

    public function edit(Request $request)
    {
        $rules = [
            'id' => 'required|exists:users',
            'username' => [ 'required', 'min:3', 'max:64', Rule::unique('users')->ignore($request->id) ],
            'originalUsername' => 'required|min:3|max:64|exists:users,username',
            'email' => [ 'required', 'email', 'max:256', Rule::unique('users')->ignore($request->id) ],
            'originalEmail' => 'required|email|max:256|exists:users,email',
            'language' => 'required|min:2|max:2',
            'isConfirmed' => 'boolean',
            'roles' => 'array',
            'roles.*' => 'required',
        ];

        $messages = [
            'id.required' => 'validation.identifierDoesNotExist',
            'id.exists' => 'validation.identifierDoesNotExist',
            'username.required' => 'validation.requiredField',
            'username.min' => 'validation.mustBeAtLeast3CharsLong',
            'username.max' => 'validation.mustBe64CharsOrFewer',
            'username.unique' => 'validation.usernameAlreadyExists',
            'originalUsername.required' => 'validation.requiredField',
            'originalUsername.min' => 'validation.mustBeAtLeast3CharsLong',
            'originalUsername.max' => 'validation.mustBe64CharsOrFewer',
            'originalUsername.exists' => 'validation.usernameDoesNotExist',
            'email.required' => 'validation.requiredField',
            'email.email' => 'validation.invalidEmail',
            'email.max' => 'validation.mustBe256CharsOrFewer',
            'email.unique' => 'validation.emailAlreadyExists',
            'originalEmail.required' => 'validation.requiredField',
            'originalEmail.email' => 'validation.invalidEmail',
            'originalEmail.max' => 'validation.mustBe256CharsOrFewer',
            'originalEmail.exists' => 'validation.emailDoesNotExist',
            'language.required' => 'validation.requiredField',
            'language.min' => 'validation.mustBe2Chars',
            'language.max' => 'validation.mustBe2Chars',
            'isConfirmed.boolean' => 'validation.requiredField',
            'roles.required' => 'validation.requiredField',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, $validator->errors());
        }

        if ($request->id === 1) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, null);
        }

        // Validate language
        $isLanguageFound = false;
        foreach ($this->enumController->getLanguages() as $lang) {
            if ($request->language === $lang) {
                $isLanguageFound = true;
                break;
            }
        }

        if (!$isLanguageFound) {
            return $this->response(
                false,
                'error.validationError',
                [],
                Response::HTTP_OK,
                [ 'language' => ['validation.requiredField'] ]
            );
        }

        $dateTime = Carbon::now();

        $user = User::find($request->id);
        $user->username = $request->username;
        $user->email = $request->email;
        $user->language = $request->language;
        $user->confirmed = $request->isConfirmed;
        $user->editedAt = $dateTime->toDateTimeString();
        $updated = $user->save();

        if (!$updated) {
            return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, null);
        }

        $roleErrors = RoleController::storeExistingUserRoles($request->roles, $user->id);

        return $this->response(true, 'dataUpdated', ['userId' => $user->id], Response::HTTP_OK, $roleErrors);
    }

    public function checkUserRole($id, $role)
    {
        $user = User::find($id);
        $userRoles = Role::where('userId', $user->id)->get();

        return $this->response(true, '', ['hasRole' => $this->checkIfUserHasRole($userRoles, $role)]);
    }

    public function checkIfUserHasRole($userRoles, $role)
    {
        foreach ($userRoles as $userRole) {
            if ($userRole['role'] === $role || $userRole['role'] === RoleController::SUPER_ADMIN) {
                return true;
            }
        }

        return false;
    }
}
