<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RoleController extends Controller
{
    const SUPER_ADMIN = 'superAdmin';
    const ADD_NEW_USER = 'addNewUser';
    const SEE_STATISTICS = 'seeStatistics';
    const ENTER_ADMIN_CP = 'enterAdminCp';
    const MARK_USER = 'markUser';
    const EDIT_USER = 'editUser';
    const READ_MESSAGES = 'readMessages';
    const REPLY_ON_MESSAGES = 'replyOnMessages';

    const ROLES = array(
        self::SUPER_ADMIN,
        self::ADD_NEW_USER,
        self::SEE_STATISTICS,
        self::ENTER_ADMIN_CP,
        self::MARK_USER,
        self::EDIT_USER,
        self::READ_MESSAGES,
        self::REPLY_ON_MESSAGES,
    );

    public static function storeNewUserRoles($roles, $userId)
    {
        $roleErrors = [ 'roles' => [] ];

        if (!is_array($roles)) {
            return $roleErrors;
        }

        foreach ($roles as $key => $chosenRole) {
            if (!self::isValidRole($chosenRole)) {
                continue;
            }

            $created = self::storeRole($chosenRole, $userId);

            if (!$created) {
                $roleErrors['roles'][] = [ $key => 'validation.errorAddingRole' ];
                continue;
            }
        }

        return $roleErrors;
    }

    public static function storeExistingUserRoles($roles, $userId)
    {
        $roleErrors = [ 'roles' => [] ];

        if (!is_array($roles)) {
            return $roleErrors;
        }

        $userActiveRoles = Role::where('userId', $userId)->get();

        $listOfRoleIdsThatNeedToBeDeleted = [];
        foreach ($userActiveRoles as $record) {
            $listOfRoleIdsThatNeedToBeDeleted[] = $record['id'];
        }

        foreach ($roles as $key => $chosenRole) {
            if (!self::isValidRole($chosenRole)) {
                continue;
            }

            $found = false;
            foreach ($userActiveRoles as $record) {
                if ($record['role'] === $chosenRole) {
                    $listOfRoleIdsThatNeedToBeDeleted = array_diff($listOfRoleIdsThatNeedToBeDeleted, [$record['id']]);
                    $found = true;
                    break;
                }
            }

            if (!$found) {
                $created = self::storeRole($chosenRole, $userId);

                if (!$created) {
                    $roleErrors['roles'][] = [ $key => 'validation.errorAddingRole' ];
                    continue;
                }
            }
        }

        foreach ($listOfRoleIdsThatNeedToBeDeleted as $deletableId) {
            try {
                $role = Role::findOrFail($deletableId);
            } catch (ModelNotFoundException $e) {
                $roleErrors['roles'][] = [ $deletableId => 'validation.errorDeletingRole' ];
                continue;
            }

            $deleted = $role->delete();

            if (!$deleted) {
                $roleErrors['roles'][] = [ $deletableId => 'validation.errorDeletingRole' ];
                continue;
            }
        }

        return $roleErrors;
    }

    private static function isValidRole($chosenRole)
    {
        foreach (self::ROLES as $role) {
            if ($chosenRole === $role) {
                return true;
            }
        }

        return false;
    }

    private static function storeRole($role, $userId)
    {
        $roleObj = new Role();
        $roleObj->userId = $userId;
        $roleObj->role = $role;
        $created = $roleObj->save();

        return $created;
    }
}
