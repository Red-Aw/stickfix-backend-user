<?php

namespace App\Http\Controllers;

use App\Services\EnumService;

class EnumController extends Controller
{
    private $enumService;

    public function __construct(EnumService $enumService)
    {
        $this->enumService = $enumService;
    }

    public function getLanguages()
    {
        $response = json_decode($this->enumService->getLanguages(), true);
        $languages = [];
        if (is_array($response) && array_key_exists('data', $response)) {
            $response = $response['data'];
            if (is_array($response) && array_key_exists('LANGUAGES', $response)) {
                $languages = $response['LANGUAGES'];
            }
        }

        return $languages;
    }
}
