<?php

namespace App\Services;

use App\Traits\RequestService;
use function config;

class EnumService
{
    use RequestService;

    protected $baseUri;
    protected $secret;

    public function __construct()
    {
        $this->baseUri = config('services.enums.baseUri');
        $this->secret = config('services.enums.secret');
    }

	public function getLanguages() : string
    {
        return $this->request('GET', '/api/getLanguages');
    }
}
