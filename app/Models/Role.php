<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'userId',
        'role',
        'addedAt',
    ];

    public function author()
    {
        return $this->belongsTo(User::class, 'id');
    }
}
